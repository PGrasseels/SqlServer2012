﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlServer2012
{
    public partial class SqlCx
    {

        public SqlDataReader QueryReader(string Query){

            Cx_Cmd = new SqlCommand(Query, Cx);
            SqlDataReader QueryReader = Cx_Cmd.ExecuteReader();
            return QueryReader;
        }

    }
}
