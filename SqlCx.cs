﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlServer2012
{
    public partial class SqlCx
    {
        SqlConnection Cx;
        SqlCommand Cx_Cmd;

        public SqlCx(string C, bool Init)
        {
            Cx = new SqlConnection(C);
            if (Init)
            {
                Cx.Open();
            }
        }


        public void OpenCx()
        {
            try
            {
                Cx.Open();
            }
            catch (Exception E)
            {
                Trace.Write(E.Message);
            }
            finally
            {
                try
                {
                    Cx.Close();
                }
                catch (Exception E)
                {
                    Trace.Write(E.Message);
                }
            }
        }

        public void CloseCx()
        {
            try
            {
                Cx.Close();
            }
            catch (Exception E)
            {
                Trace.Write(E.Message);
            }
        }

        public bool BOpenCx()
        {
            try
            {
                Cx.Open();
                return true;
            }
            catch (Exception E)
            {
                Trace.Write(E.Message);
                return false;
            }
            finally
            {
                try
                {
                    Cx.Close();
                }
                catch (Exception E)
                {
                    Trace.Write(E.Message);
                }
            }
        }

        public bool BCloseCx()
        {
            try
            {
                Cx.Close();
                return true;
            }
            catch (Exception E)
            {
                Trace.Write(E.Message);
                return false;
            }
        }

    }
}
